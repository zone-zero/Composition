package composition.localization;

import java.util.ResourceBundle;

import composition.stats.DebugX;

public class Locale {
	
	private java.util.Locale localLocale;
	private ResourceBundle localM;
	private String localL;
	private String localC;
	
	private static java.util.Locale locale;
	private static ResourceBundle m;
	private static String l;
	private static String c;
	private static boolean globalWorking = false;

	/**
	 * Initializes the locale.
	 * @param x the name of the properties list.
	 * @param lang the language to be used by the Locale.
	 * @param coun the country associated with the language.
	 */
	public Locale(String x, String lang, String coun) {
		try {
			DebugX.writeLine("Initializing locale for " + lang + "_" + coun);
			localL = lang;
			localC = coun;
			localLocale = new java.util.Locale(lang, coun);
			localM = ResourceBundle.getBundle(x, localLocale);
			localL = lang;
			DebugX.writeLine("Locale initialized.");
		} catch (Exception ex) {
			DebugX.writeErr("Could not start a local Locale", ex);
		}
	}
	
	/**
	 * Gets and returns the name of the locale country.
	 * @return the country of the locale.
	 */
	public String getCountry() {
		return localC;
	}
	
	public static String getGlobalCountry() {
		return c;
	}
	
	public static String getGlobalLanguage() {
		return l;
	}
	
	public static String getGlobalMessage(String x) {
		if (globalWorking) {
			try {
				return m.getString(x);
			} catch (Exception ex) {
				DebugX.writeErr("Could not get the message", ex);
				return "";
			}
		} else {
			DebugX.writeErr("Global message requested; Locale not initialized.");
			return "";
		}
	}
	
	/**
	 * Gets and returns the name of the locale language.
	 * @return the language of the locale.
	 */
	public String getLanguage() {
		return localL;
	}
	
	/**
	 * Gets and returns a localized message.
	 * @param m the name of the message.
	 * @return a String containing the localized message.
	 */
	public String getMessage(String m) {
		try {
			return localM.getString(m);
		} catch (Exception ex) {
			DebugX.writeErr("Could not get the message", ex);
			return "";
		}
	}
	
	/**
	 * Starts and activates the global locale.
	 * @param x the name of the locale file.
	 * @param lang a String containing the name of the language.
	 * @param coun a String containing the name of the country.
	 */
	public static void start(String x, String lang, String coun) {
		try {
			DebugX.writeLine("Initializing global locale for " + lang + "_" + coun);
			l = lang;
			c = coun;
			locale = new java.util.Locale(l, c);
			m = ResourceBundle.getBundle(x, locale);
			globalWorking = true;
			DebugX.writeLine("Global locale initialized.");
		} catch (Exception ex) {
			DebugX.writeErr("Could not start a global Locale", ex);
		}
	}
	
}
