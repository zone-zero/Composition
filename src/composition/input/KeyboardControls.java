package composition.input;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import composition.graphics.GraphicsCore;
import composition.stats.DebugX;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public abstract class KeyboardControls {

    public static final String[] STRING_BLACKLIST = new String[] { "\u000f", "\u007F", "\u001B" };

	private GraphicsCore g;
	private Map<KeyCode, Boolean> keys = new ConcurrentHashMap<KeyCode, Boolean>();
	private Map<String, Boolean> keyChars = new ConcurrentHashMap<String, Boolean>();
	private Map<KeyCode, Boolean> already = new ConcurrentHashMap<KeyCode, Boolean>();
	private boolean debug = true;
	
	public abstract void eventOnKeyReleased(KeyEvent e);
	public abstract void eventOnKeyPressed(KeyEvent e);
	public abstract void eventOnKeyTyped(KeyEvent e);
	
	public static void placeAllKeys(Map<KeyCode, Boolean> k) {
		for (KeyCode e : KeyCode.values()) {
			k.put(e, false);
		}
	}
	
	public static void placeAllChars(Map<String, Boolean> k) {
		char[] alphabet = "abcdefghijklmnopqrstuvwxyzABCDEHIJKLMNOPQSTUVWXYZ!@#$%^&*()_+~{}|\":?><,./;'[]\\~1234567890-= ".toCharArray();
		for (char x : alphabet) {
			k.put(String.valueOf(x), false);
		}
	}
	
	public KeyboardControls(GraphicsCore gfx) {
		g = gfx;
		
		placeAllKeys(keys);
		placeAllKeys(already);
		placeAllChars(keyChars);
		
		g.getCanvas().setOnKeyPressed(new EventHandler<KeyEvent>() {

			public void handle(KeyEvent e) {
				onKeyPressed(e);
			}
			
		});
		
		g.getCanvas().setOnKeyReleased(new EventHandler<KeyEvent>() {

			public void handle(KeyEvent e) {
				onKeyReleased(e);
			}
			
		});
		
		g.getCanvas().setOnKeyTyped(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				onKeyTyped(event);
			} });

		g.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {

			public void handle(KeyEvent e) {
				onKeyPressed(e);
			}
			
		});
		
		g.getScene().setOnKeyReleased(new EventHandler<KeyEvent>() {

			public void handle(KeyEvent e) {
				onKeyReleased(e);
			}
			
		});
		
		g.getScene().setOnKeyTyped(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				onKeyTyped(event);
			} });
	}
	
	private void onKeyTyped(KeyEvent ev) {
		try {
			String c = ev.getCharacter();
			if (debug) DebugX.writeLine("User typed key " + ev.getCharacter());
			// TODO Character blacklist.

            for (String x : STRING_BLACKLIST) {
                if (debug) DebugX.writeLine("Blacklisted key typed.");
                if (x.equals(c)) return;
            }

			eventOnKeyTyped(ev);
			keyChars.put(c, true);
			
		} catch (Exception ex) {
			DebugX.writeErr("Could not update key status [" + ev.getCode().getName() + "]", ex);
		}
	}
	
	private void onKeyPressed(KeyEvent ev) {
		try {
			if (debug) DebugX.writeLine("User pressed key " + ev.getCode().getName());
			if (!(already.get(ev.getCode()))) {
				already.put(ev.getCode(), true);
			}
			keys.put(ev.getCode(), true);
			eventOnKeyPressed(ev);
		} catch (Exception ex) {
			DebugX.writeErr("Could not update key status [" + ev.getCode().getName() + "]", ex);
		}
	}
	
	private void onKeyReleased(KeyEvent ev) {
		try {
			if (debug) DebugX.writeLine("User released key " + ev.getCode().getName());
			if (already.get(ev.getCode())) {
				already.put(ev.getCode(), false);
			}
			eventOnKeyReleased(ev);
		} catch (Exception ex) {
			DebugX.writeErr("Could not update key status [" + ev.getCode().getName() + "]", ex);
		}
	}
	
	public Map<String, Boolean> getMapStrings() {
		return keyChars;
	}
	
	public Map<KeyCode, Boolean> getMap() {
		return keys;
	}
	
	public boolean getPressed(KeyCode c) {
		return keys.get(c);
	}
	
}
