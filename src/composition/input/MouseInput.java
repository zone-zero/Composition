package composition.input;

import composition.graphics.GraphicsCore;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public abstract class MouseInput {

	private GraphicsCore core;
	
	private double mouseX;
	private double mouseY;
	
	public abstract void onClick(MouseEvent ev);
	public abstract void onMove(MouseEvent ev);
	
	public MouseInput(GraphicsCore gfx) {
		core = gfx;
		
		core.getCanvas().setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				
			}
			
		});
		
		core.getCanvas().setOnMouseReleased(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				
			}
			
		});
		
		core.getScene().setOnMouseMoved(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				mouseMoved(event);
			}
			
		});

		core.getScene().setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				
			}
			
		});
		
		core.getScene().setOnMouseMoved(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				mouseMoved(event);
			}
			
		});
		
		core.getScene().setOnMousePressed(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				mouseClicked(event);
			}
			
		});
		
		core.getScene().setOnMouseReleased(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				
			}
			
		});
	}
	
	public double getMousePosX() {
		return mouseX;
	}
	
	public double getMousePosY() {
		return mouseY;
	}
	
	private void mouseMoved(MouseEvent ev) {
		mouseX = ev.getSceneX();
		mouseY = ev.getSceneY();
		onMove(ev);
	}
	
	private void mouseClicked(MouseEvent ev) {
		if (ev.isPrimaryButtonDown()) onClick(ev);
	}
}
