/**
 * 
 */
package composition.screen;

import java.util.Collection;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

import composition.animation.Animation;
import composition.components.Component;
import composition.graphics.GraphicsCore;
import composition.input.KeyboardControls;
import composition.input.MouseInput;
import composition.overlay.Overlay;
import composition.stats.DebugX;
import javafx.animation.AnimationTimer;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 * This class contains methods and functions that manage screen development.
 * @author LucioleDeux
 * @since Composite a1.2
 */
public abstract class CompositeScreen {
	
	// TODO Documentation

	private ConcurrentHashMap<String, Animation> animations = new ConcurrentHashMap<String, Animation>();
	private Color background = Color.CORNFLOWERBLUE;
	private ConcurrentHashMap<String, Component> components = new ConcurrentHashMap<String, Component>();
	private boolean focused = false;
	private GraphicsCore gr;
	private boolean initialized = false;
	private ConcurrentHashMap<String, Overlay> overlays = new ConcurrentHashMap<>();
	private AnimationTimer timer;
	
	private MouseInput mouse;
	private KeyboardControls key;
	
	// Abstract methods
	
	/**
	 * This method is called to initialize the screen.
	 */
	public abstract void onInitialize();

	/**
	 * This method is called when the program crashes.
	 */
	public abstract void onCrash();

	/**
	 * This method is called to draw objects on screen.
	 */
	public abstract void onDraw();

	/**
	 * This method is called to update statistics on screen.
	 */
	public abstract void onUpdate();

	/**
	 * This method is called when the window is refocused.
	 */
	public abstract void onRefocus();

	/**
	 * This method is called when the screen buffer is started.
	 */
	public abstract void onStart();

    /**
     * This method is called when the screen buffer is stopping and the window is closing.
     */
	public abstract void onStop();

    /**
     * This method is called when the window is unfocused.
     */
	public abstract void onUnfocus();

    /**
     * This method is called when a mouse clicks the window.
     * @param e The mouse event that provides information about a mouse click.
     */
	public abstract void onMouseClicked(MouseEvent e);

    /**
     * This method is called when a mouse moves within the window.
     * @param e The mouse event that provides information about a mouse movement.
     */
	public abstract void onMouseMoved(MouseEvent e);

    /**
     * This method is called when a key is pressed.
     * @param e The key event that provides information about a key press.
     */
	public abstract void onKeyPressed(KeyEvent e);

    /**
     * This method is called when a key is released.
     * @param e The key event that provides information about a key release.
     */
	public abstract void onKeyReleased(KeyEvent e);

    /**
     * The method is called when a key is typed.
     * @param e The key event that provides information about when a key is typed.
     */
	public abstract void onKeyTyped(KeyEvent e);
	
	
	/**
	 * Initializes the {@linkplain CompositeScreen}.
	 */
	public CompositeScreen() {
		try {
			
		} catch (Exception ex) {
			DebugX.writeErr("Could not initialize", ex);
		}
	}
	
	/**
	 * Calls the {@link #onCrash()} method and forces the screen to come to a
	 * halt, stopping it from drawing.
	 */
	public void crash() {
		if (initialized) {
			for (Animation x : animations.values()) x.stop();
			timer.stop();
			onCrash();
		}
	}
	
	/**
	 * Draws the screen.
	 */
	public void draw() {
		getCore().clear(background);
		if (initialized) {
			onDraw();
		}
		
		for (Component x : components.values()) x.draw();
		for (Overlay x : overlays.values()) x.draw();
	}
	
	/**
	 * Gets and returns an {@linkplain Animation} requested by its name.
	 * @param name the name of the {@linkplain Animation}.
	 * @return an {@linkplain Animation} that was requested.
	 */
	public Animation getAnimation(String name) {
		return animations.get(name);
	}
	
	/**
	 * Gets and returns a {@linkplain Component} requested by its name.
	 * @param name the name of the {@linkplain Component}.
	 * @return a {@linkplain Component} that was requested.
	 */
	public Component getComponent(String name) {
		return components.get(name);
	}
	
	/**
	 * Adds a {@linkplain Component} to the master list of the components that
	 * are a part of the screen.
	 * @param c the {@linkplain Component} that would be added to the list of
	 * components.
	 */
	public void addComponent(Component c) {
		components.put(c.getName(), c);
	}
	
	/**
	 * Gets and returns the {@linkplain GraphicsCore} used to draw on the screen. 
	 * @return the {@linkplain GraphicsCore} used for drawing.
	 */
	public GraphicsCore getCore() {
		return gr;
	}
	
	/**
	 * Gets and returns the {@linkplain KeyboardControls} that is created on the
	 * {@linkplain CompositeScreen} instance.
	 * @return Returns the {@linkplain KeyboardControls} that is used to detect keyboard events.
	 */
	public KeyboardControls getKeyboard() {
		return key;
	}
	
	/**
	 * Gets and returns the {@linkplain MouseInput} used to detect mouse movement.
	 * @return Returns the {@linkplain MouseInput} used to detect mouse movement.
	 */
	public MouseInput getMouse() {
		return mouse;
	}
	
	/**
	 * Gets and returns the {@linkplain AnimationTimer} that is used to allow
	 * animations and updating of the screen.
	 * @return the {@linkplain AnimationTimer} that is used to draw and update.
	 */
	public AnimationTimer getTimer() {
		return timer;
	}
	
	/**
	 * Gets and returns a {@linkplain Boolean} that is valued depending on if the
	 * {@linkplain CompositeScreen} has been initialized.
	 * @return {@code true} if the {@linkplain CompositeScreen} has been initialized.
	 */
	public boolean getInitialized() {
		return initialized;
	}
	
	/**
	 * Initializes the {@linkplain CompositeScreen}, assigning the instance a
	 * {@linkplain GraphicsCore} which would be used to draw.
	 * @param g a {@linkplain GraphicsCore} that would be used to draw.
	 */
	public void initialize(GraphicsCore g) {
		try {
			DebugX.writeLine("Initializing " + getClass().getSimpleName() + "...");
			gr = g;
			onInitialize();
			initialized = true;
			
			mouse = new MouseInput(gr) {

				@Override
				public void onClick(MouseEvent ev) {
					for (Component x : getAllComponents()) {
						if (x.contains(ev.getSceneX(), ev.getSceneY()) && focused){
							if (focused) DebugX.writeLine("User clicked on [" + x.getName() + "]");
							if (focused) x.interact();
						}
					}
					if (focused) onMouseClicked(ev);
				}

				@Override
				public void onMove(MouseEvent ev) {
					if (focused) onMouseMoved(ev);
				}
				
			};
			
			key = new KeyboardControls(gr) {

				@Override
				public void eventOnKeyReleased(KeyEvent e) {
					if (focused) {
					    onKeyPressed(e);
					    for (Overlay x : overlays.values()) {
					        x.onKeyPressed(e);
                        }
                    }
				}

				@Override
				public void eventOnKeyPressed(KeyEvent e) {
					if (focused) onKeyReleased(e);
                    for (Overlay x : overlays.values()) {
                        x.onKeyReleased(e);
                    }
				}

				@Override
				public void eventOnKeyTyped(KeyEvent e) {
					if (focused) onKeyTyped(e);
                    for (Overlay x : overlays.values()) {
                        x.onKeyTyped(e);
                    }
				}
				
			};
			
			DebugX.writeLine("Initialized.");
		} catch (Exception ex) {
			DebugX.writeErr("Could not start GraphicsCore", ex);
		}
	}
	
	/**
	 * Sets an {@linkplain Animation} within the array of Animations, assigning it a
	 * name that would be used to identify the specific Animation, apart from the
	 * other Animations that may be used within the instance.
	 * @param x the name of the {@linkplain Animation}.
	 * @param y the specific {@linkplain Animation}.
	 */
	public void setAnimation(String x, Animation y) {
		animations.put(x, y);
	}
	
	/**
	 * Starts a specific animation to begin playing.
	 * @param x the identifier for the specific animation.
	 */
	public void playAnimation(String x) {
		animations.get(x).start();
	}

    /**
     * Gets and returns a {@linkplain Collection} of {@linkplain Animation}.
     * @return Returns a {@linkplain Collection} of {@linkplain Animation}.
     */
	public Collection<Animation> getAllAnimations() {
		return animations.values();
	}

    /**
     * Gets and returns a {@linkplain Collection} of {@linkplain Component}.
     * @return Returns a {@linkplain Collection} of {@linkplain Component}.
     */
	public Collection<Component> getAllComponents() {
		return components.values();
	}

    /**
     * Gets and returns the {@link ConcurrentHashMap} that contains the components.
     * @return Returns the {@link ConcurrentHashMap} that contains the components.
     */
	public ConcurrentHashMap<String, Component> getComponentMap() {
		return components;
	}

    /**
     * Gets and returns an {@linkplain Enumeration} that contains {@linkplain String} that
     * includes the Components used in the screen.
     * @return Returns an {@linkplain Enumeration} with component names.
     */
	public Enumeration<String> getAllComponentNames() {
		return components.keys();
	}

    /**
     * Refocuses the window.
     */
	public void refocus() {
		if (getInitialized()) {
			DebugX.writeLine("Refocusing window...");
			for (Animation a : getAllAnimations()) {
				a.getTimeline().play();
			}

			onRefocus();
			focused = true;
			timer.start();
		}
	}

    /**
     * Unfocuses the window.
     */
	public void unfocus() {
		if (getInitialized()) {
			DebugX.writeLine("Unfocusing window...");
			for (Animation a : getAllAnimations()) {
				a.getTimeline().stop();
			}

			onUnfocus();
			focused = false;
			timer.stop();
		}
	}
	
	/**
	 * Starts the screen buffer, allowing the {@linkplain CompositeScreen} to be drawn.
	 * @see #stop()
	 */
	public void start() {
		if (getInitialized()) {

			timer = new AnimationTimer() {
				public void handle(long arg0) {
					try {
						draw();
						onUpdate();
						updateComponents();
					} catch (Exception ex) {
						DebugX.writeErr("Draw error", ex);
						timer.stop();
					}
				}
			};
			
			focused = true;
			timer.start();
			onStart();
		}
	}

    /**
     * Reinitializes all the components drawn on the screen.
     */
	public void updateComponents() {
		for (Component x : getAllComponents()) {
			x.init(this, x.getChoice());
		}
	}
	
	/**
	 * Stops the screen buffer, closing the {@linkplain CompositeScreen}. This is usually
	 * called by {@linkplain composition.screen.ScreenStarter#start(CompositeScreen, boolean) ScreenStarter.start(CompositeScreen, boolean)}.
	 */
	public void stop() {
		if (getInitialized()) {
			for (Animation a : getAllAnimations()) {
				a.getTimeline().stop();
			}
			
			focused = false;
			timer.stop();
			onStop();
		}
	}

    /**
     * Gets and returns the background.
     * @return
     */
	public Color getBackground() {
		return background;
	}

    /**
     * Sets the background color used in the CompositeScreen.
     * @param c The color that would be used in the background of the CompositeScreen.
     */
	public void setBackground(Color c) {
		background = c;
	}

	public void addOverlay(Overlay overlay) {
	    overlays.put(overlay.getName(), overlay);
    }
	
}
