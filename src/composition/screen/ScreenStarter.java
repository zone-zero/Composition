package composition.screen;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import composition.animation.Animation;
import composition.graphics.GraphicsCore;
import composition.stats.DebugX;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;

/**
 * This class contains public methods and functions that provide and maintain
 * easy switching of {@link composition.screen.Screen Screen} classes.
 * @author LucioleDeux
 *
 */
public final class ScreenStarter {

	private static CompositeScreen oldScreen;
	private static CompositeScreen currentScreen;
	private static GraphicsContext gc;
	private static GraphicsCore gr;
	private static boolean isInitialized = false;
	private static Set<CompositeScreen> screens = Collections.newSetFromMap(new ConcurrentHashMap<CompositeScreen, Boolean>());
	private static Scene s;

	public static void init(GraphicsCore x) {
		DebugX.writeLine("Starting the [ScreenStarter]...");
		s = x.getScene();
		gc = x.getGraphicsContext();
		gr = x;
		isInitialized = true;
	}
	
	public static void crash() {
		for (CompositeScreen s : screens) {
			for (Animation a : s.getAllAnimations()) a.stop();
			s.crash();
			s.stop();
		}
	}
	
	/**
	 * Gets and returns the GraphicsContext that is being used by all the screens.
	 * @return the GraphicsContext that is being used by all the screens.
	 */
	public static GraphicsContext getGraphicsContext() {
		if (isInitialized)
			return gc;
		else
			return null;
	}
	
	/**
	 * Gets and returns the GraphicsCore that is being used by all the screens.
	 * @return the GraphicsCore that is being used by all the screens.
	 */
	public static GraphicsCore getGraphicsCore() {
		if (isInitialized)
			return gr;
		else
			return null;
	}
	
	/**
	 * Gets and returns the Scene that the primary stage is using.
	 * @return the Scene that the primary stage is using.
	 */
	public static Scene getScene() {
		if (isInitialized)
			return s;
		else
			return null;
	}
	
	public static void flushOldScreen() {
		if (oldScreen != null) oldScreen.stop();
	}
	
	public static void start(CompositeScreen s) {
		try {
			if (isInitialized) {
				DebugX.writeLine("Sent screen [" + s.getClass().getSimpleName() + "] to main Composite");
				
				if (currentScreen != null) {
					oldScreen = currentScreen;
					oldScreen.stop();
				}
				
				currentScreen = s;
				screens.add(s);

				if (!s.getInitialized()) s.initialize(getGraphicsCore());
				s.start();
			}
		} catch (Exception ex) {
			DebugX.writeErr("Could not send screen [" + s.getClass().getSimpleName() + "]", ex);
		}
	}
	
	public static void start(CompositeScreen s, boolean stop) {
		try {
			if (isInitialized) {
				DebugX.writeLine("Sent screen [" + s.getClass().getSimpleName() + "] to main Composite");
				
				oldScreen = currentScreen;
				
				if (stop) oldScreen.stop();
				else oldScreen.unfocus();
				
				currentScreen = s;
				screens.add(s);
				
				if (!s.getInitialized()) s.initialize(getGraphicsCore());
				s.start();
			}
		} catch (Exception ex) {
			DebugX.writeErr("Could not send screen [" + s.getClass().getSimpleName() + "]", ex);
		}
	}
	
	public static void moveBack() {
		try {
			if (isInitialized) {
				DebugX.writeLine("Sent screen [" + oldScreen.getClass().getSimpleName() + "] to main Composite");
				
				CompositeScreen x = currentScreen;
				CompositeScreen y = oldScreen;
				
				x.getTimer().stop();
				x.stop();
				oldScreen = x;
				
				currentScreen = y;
				y.refocus();
			}
		} catch (Exception ex) {
			DebugX.writeErr("Could not send screen [" + oldScreen.getClass().getSimpleName() + "]", ex);
		}
	}
	
}
