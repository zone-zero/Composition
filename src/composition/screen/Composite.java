package composition.screen;

import composition.graphics.GraphicsCore;
import composition.stats.DebugX;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * An {@code abstract} class which manages creating and drawing on a JavaFX.
 * @author LucioleDeux
 *
 */
public abstract class Composite extends Application {

	
	// Private objects
	
	private AnchorPane pane = new AnchorPane();
	private Scene scene;
	private Canvas canvas;
	private GraphicsContext gc;
	public GraphicsCore gr;
	
	private String title = "Untitled Composite window";
	private double width = 800;
	private double height = 600;
	
	/**
	 * This method is called upon the initialization of a {@linkplain Composite}.
	 */
	public abstract void onInit();
	
	/**
	 * This method is called upon the opening of a {@linkplain Composite}.
	 * @param s
	 */
	public abstract void onStart(Stage s);
	
	// Method to list debug information on Composition creation.
	private void compi() {
		DebugX.writeLine("Starting DebugX...");
		DebugX.writeLine("[PROPERTY] user.dir   == " + System.getProperty("user.dir"));
		DebugX.writeLine("[PROPERTY] java.home  == " + System.getProperty("java.home"));
		DebugX.writeLine("[PROPERTY] os.name    == " + System.getProperty("os.name"));
		DebugX.writeLine("[PROPERTY] os.version == " + System.getProperty("os.version"));
		
		onInit();
	}
	
	/**
	 * This method is used to create a {@linkplain Composite}, initializing the Composite
	 * with basic information.
	 */
	public Composite() {
		compi();
	}
	
	/**
	 * This method is used to create a {@linkplain Composite}, initializing the Composite
	 * with basic information and a title.
	 * @param t a String that contains the title of the {@linkplain Composite} window.
	 */
	public Composite(String t) {
		title = t;
		compi();
	}
	
	/**
	 * This method is used to create a {@linkplain Composite}, initializing the Composite
	 * with basic information, a width, and a height.
	 * @param w a {@linkplain Double} that contains the width of the {@linkplain Composite}.
	 * @param h a {@linkplain Double} that contains the height of the {@linkplain Composite}.
	 */
	public Composite(double w, double h) {
		width = w;
		height = h;
		compi();
	}

	/**
	 * This method is used to create a {@linkplain Composite}, initializing the Composite
	 * with basic information, a title, a width, and a height.
	 * @param t a String that contains the title of the {@linkplain Composite} window.
	 * @param w a {@linkplain Double} that contains the width of the {@linkplain Composite}.
	 * @param h a {@linkplain Double} that contains the height of the {@linkplain Composite}.
	 */
	public Composite(String t, double w, double h) {
		title = t;
		width = w;
		height = h;
		compi();
	}
	
	/**
	 * Gets and returns the main {@linkplain GraphicsCore} that the {@linkplain Composite} uses
	 * for drawing within the window.
	 * @return the {@linkplain GraphicsCore} used for drawing on the {@linkplain Composite}.
	 */
	public GraphicsCore getCore() {
		return gr;
	}
	
	/**
	 * Sets the width and height of the {@linkplain Composite}.
	 * @param w a {@linkplain Double} that contains the new width of the {@linkplain Composite}.
	 * @param h a {@linkplain Double} that contains the new height of the {@linkplain Composite}.
	 */
	public void setSize(double w, double h) {
		width = w;
		height = h;
	}
	
	/**
	 * Sets the title of the {@linkplain Composite}.
	 * @param t a String that contains the title of the {@linkplain Composite} window.
	 */
	public void setTitle(String t) {
		title = t;
	}
	
	public void start(Stage s) {
		DebugX.writeLine("Setting up [Composite]...");
		DebugX.writeLine("Creating scene");
		scene = new Scene(pane, width, height);
		DebugX.writeLine("Creating canvas");
		canvas = new Canvas(width, height);
		DebugX.writeLine("Creating GraphicsContext2D");
		gc = canvas.getGraphicsContext2D();

		DebugX.writeLine("Creating GraphicsCore");
		gr = new GraphicsCore(gc,scene,canvas,s);
		gr.clear();
		
		ScreenStarter.init(this.gr);
		
		s.setTitle(title);
		pane.getChildren().add(canvas);
		AnchorPane.setBottomAnchor(canvas, 0.0);
		AnchorPane.setLeftAnchor(canvas, 0.0);
		AnchorPane.setRightAnchor(canvas, 0.0);
		AnchorPane.setTopAnchor(canvas, 0.0);
		s.setScene(scene);

		DebugX.writeLine("Finished setting up [Composite]...");
		s.show();
		onStart(s);
	}
	
}
