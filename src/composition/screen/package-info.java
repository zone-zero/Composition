
/**
 * 
 * This package includes classes and interfaces used to manage the active JavaFX
 * screen and the Graphics components used to specify a screen that would be
 * shown on a Composite.
 * @author LucioleDeux
 *
 */
package composition.screen;