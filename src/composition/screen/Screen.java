package composition.screen;

import composition.graphics.GraphicsCore;

/**
 * This interface contains the methods and variables needed for a
 * screen to be called.
 * @author LucioleDeux
 * @deprecated
 */
public interface Screen {

	/**
	 * Draws the Screen on the GraphicsCore specified in {@link #initialize(GraphicsCore)}.
	 */
	public void draw();
	
	/*
	 * 
	 */
	public void update();
	
	/*
	 * 
	 */
	public void onCrash();
	
	/**
	 * Initializes the Screen to allow the method {@link #draw()} to be used.
	 * @param g the {@link composition.graphics.GraphicsCore GraphicsCore} that the Screen
	 * would use to draw on.
	 */
	public void initialize(GraphicsCore g);
	
}
