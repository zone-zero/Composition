
/**
 * Contains all the packages used to create a {@linkplain Composite} program.
 * @author LucioleDeux
 *
 */
package composition;