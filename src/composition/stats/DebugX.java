package composition.stats;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class contains methods and variables used for debugging a Composition
 * application.
 * @author LucioleDeux
 *
 */
public class DebugX {

	/**
	 * The debug log.
	 */
	private static String log = "[" + getCurrentTime() + "] [" + DebugX.class.getSimpleName() + "]: -- Debugging begins here --";
	
	/**
	 * The current directory that is being used.
	 */
	private static String currentDirectory = System.getProperty("user.dir");
	
	/**
	 * Gets and returns the active working directory.
	 * @return a string with the URL to the active directory.
	 */
	public static String getCurrentDirectory() {
		return currentDirectory;
	}

	/**
	 * Gets and returns the current time, using a {@linkplain SimpleDateFormat}.
	 * @return the current time.
	 */
	public static String getCurrentTime() {
		SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return time.format(new Date());
	}
	
	/**
	 * Gets and returns the name of the calling class.
	 * @return the name of the calling class.
	 */
	private static String getCallerCallerClassName() { 
	    StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
	    String callerClassName = null;
	    for (int i=1; i<stElements.length; i++) {
	        StackTraceElement ste = stElements[i];
	        if (!ste.getClassName().equals(DebugX.class.getName())&& ste.getClassName().indexOf("java.lang.Thread")!=0) {
	            if (callerClassName==null) {
	                callerClassName = ste.getClassName();
	            } else if (!callerClassName.equals(ste.getClassName())) {
	                return ste.getClassName();
	            }
	        }
	    }
	    return null;
	 }
	
	/**
	 * Gets and returns the debug log.
	 * @return the debug log.
	 */
	public static String getLog() {
		return log;
	}
	
	/**
	 * Writes to the debug log and console.
	 * @param m the message that would be written.
	 * @deprecated
	 */
	public static void writeLine(DebugMessage m) {
		String msg = "[" + m.getTimeAsString() + "] [" + m.getOrigin() + "]: " + m.getMessage();
		System.out.println(msg);
		log += "\n" + msg;
	}

	/**
	 * Writes to the debug log and console.
	 * @param m the message that would be written.
	 */
	public static void writeLine(String m) {
		String origin = getCallerCallerClassName();
		String message = m;

		if (origin.equals("")) {
			String msg = "[" + getCurrentTime() + "] [UNKNOWN SOURCE]: " + message;
			System.out.println(msg);
			log += "\n" + msg;
		} else {
			String msg = "[" + getCurrentTime() + "] [" + origin + "]: " + message;
			System.out.println(msg);
			log += "\n" + msg;
		}
	}
	
	/**
	 * Writes to the error log and console.
	 * @param m the message that would be written.
	 * @deprecated
	 */
	public static void writeErr(DebugMessage m) {
		writeLine("[ERROR]" + m);
		String msg = "[" + m.getTimeAsString() + "] [" + m.getOrigin() + "]: [ERROR] " + m.getMessage();
		System.err.println(msg);
		log += "\n" + msg;
	}

	/**
	 * Writes to the error log and console.
	 * @param m the message that would be written.
	 */
	public static void writeErr(String m) {
		String origin = getCallerCallerClassName();
		String message = m;

		if (origin.equals("")) {
			String msg = "[" + getCurrentTime() + "] [UNKNOWN SOURCE]: " + message;
			System.err.println(msg);
			log += "\n" + msg;
		} else {
			String msg = "[" + getCurrentTime() + "] [" + origin + "]: " + message;
			System.err.println(msg);
			log += "\n" + msg;
		}
	}
	
	/**
	 * Writes to the error log and console, then prints the error with its stack trace.
	 * @param m the {@linkplain String} message that would be written to the log.
	 * @param e the {@linkplain Exception} that would be used to write the stack trace.
	 */
	public static void writeErr(String m, Exception e) {
		writeErr(m + ": " + e.getLocalizedMessage());
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		
		String msg = "[" + getCurrentTime() + "] [StackTrace] " + sw.toString();
		System.err.println(msg);
		log += "\n" + msg;
	}
	
}
