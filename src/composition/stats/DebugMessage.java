package composition.stats;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class contains variables and methods used to send a debugging message out
 * to the {@link composition.stats.DebugX DebugX} debugging log. This class is not
 * needed to send out a log entry in the debugging log, but should be used to offer
 * more parameters and specification to send an entry to the log.
 * 
 * @author LucioleDeux
 * @deprecated
 */
public class DebugMessage {
	
	/**
	 * This variable holds the originating class that sent out the debug log.
	 */
	private String origin;
	
	/**
	 * This variable holds the formatting for the time.
	 */
	private SimpleDateFormat time= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * This variable holds the entry contents to be sent out to the debug log.
	 */
	private String message;
	
	/**
	 * This variable holds the time in which the message was initialized.
	 */
	private String debugTime;
	
	/**
	 * This method initializes the instance, with the contents of the entry
	 * specified in the variable {@code m}.
	 * @param m a string that represents the contents of the entry to be placed
	 * in the debug log.
	 */
	public DebugMessage(String m) {
		origin = getCallingClassName();
		message = m;
		debugTime = time.format(new Date());
	}
	
	/**
	 * Gets and returns the name of the class which called the debug message to be initialized,
	 * returning no value if the name of the class which called the debug message could not
	 * be found.
	 * @return Returns the name of the class which called the debug message to be initialized.
	 */
	private String getCallingClassName() {
		StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
		
		for (int i = 1; i < stElements.length; i++) {
			StackTraceElement ste = stElements[i];
			if (!ste.getClassName().equals(DebugMessage.class.getName()) && ste.getClassName().indexOf("java.lang.Thread") != 0) {
				return ste.getClassName();
			}
		}
		
		return "";
	}
	
	/**
	 * Gets and returns a {@code boolean} which represents if the class name was successfully determined.
	 * @return Returns true if a class name could be returned in {@link #getCallingClassName()}.
	 */
	public Boolean getIfOriginExists() {
		if (origin.equals("")) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Gets and returns the name of the class which called the debug message to be initialized,
	 * returning no value if the name of the class which called the debug message could not
	 * be found.
	 * @return Returns the name of the class which called the debug message to be initialized.
	 */
	public String getOrigin() {
		return origin;
	}
	
	/**
	 * Gets and returns the contents of the debug message.
	 * @return A string with the contents of the debug message.
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Gets and returns the time format.
	 * @return Returns the time format.
	 */
	public SimpleDateFormat getTime() {
		return time;
	}
	
	/**
	 * Gets and returns the time in which the debug message was initialized.
	 * @return Returns the time in which the debug message was initialized.
	 */
	public String getTimeAsString() {
		return debugTime;
	}

}
