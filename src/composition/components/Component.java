package composition.components;

import composition.graphics.GraphicsCore;
import composition.screen.CompositeScreen;

public interface Component {

	public boolean contains(double x, double y);
	public void init(CompositeScreen s, int c);
	public void init(CompositeScreen s, int c, double x, double y, double w, double h);
	public void draw();
	public void update();
	public int getChoice();
	public GraphicsCore getCore();
	public double getHeight();
	public boolean getIsEnabled();
	public boolean getKeyboardMode();
	public double getWidth();
	public String getName();
	public double getX();
	public double getY();
	public void interact();
	public boolean isSelected();
	public void setIsEnabled(Boolean x);
	public void setKeyboardMode(boolean x);
	public void setHeight(double h);
	public void setPosition(double x, double y);
	public void setWidth(double w);
	
}
