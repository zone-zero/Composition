
/**
 * 
 * This package contains classes that are used to allow animation within a
 * {@link composite.screen.CompositeScreen CompositeScreen}.
 * @author LucioleDeux
 *
 */
package composition.animation;