package composition.animation;

import composition.screen.CompositeScreen;
import composition.stats.DebugX;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;

/**
 * This abstract class contains methods and variables used to animate objects within
 * the {@link composition.graphics.GraphicsCore GraphicsCore} and on JavaFX itself.
 * <p>
 * @author LucioleDeux
 * @since Composite a1.0
 * @version Composite a1.2
 */
public abstract class Animation {

	// Private variables
	
	private String name;
	private CompositeScreen parent;
	private Observable property;
	private Timeline timeline;
	private boolean working = false;
	
	public abstract void initialize();
	public abstract void onFinish();
	public abstract void onStart();
	
	// Public methods
	
	/**
	 * Initializes the {@linkplain Animation} class, specifying the name
	 * of the animation and also assigning the {@linkplain CompositeScreen}
	 * that the Animation belongs to.
	 * @param screen the {@linkplain CompositeScreen} that holds the Animation
	 * and that the Animation is a part of.
	 * @param name a {@linkplain String} representing the name of the Animation.
	 */
	public Animation(CompositeScreen screen, String name) {
		try {
			parent = screen;
			timeline = new Timeline();
			this.name = name;
			
			timeline.setOnFinished(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent arg0) {
					onFinish();
				}
				
			});
			
			initialize();
		} catch (Exception ex) {
			DebugX.writeErr("Could not add initialize [Animation" + name + "]", ex);
		}
	}
	
	/**
	 * Adds a {@linkplain KeyFrame} to the {@linkplain Animation}, cancelling the
	 * additional keyframe if there are errors adding the keyframe.
	 * @param k The {@link KeyFrame} that would be added onto the Animation.
	 * 
	 * @see #addKeyFrame(Duration, KeyValue...)
	 */
	public void addKeyFrame(KeyFrame k) {
		try {
			timeline.getKeyFrames().add(k);
		} catch (Exception ex) {
			DebugX.writeErr("Could not add KeyFrame to [Animation" + name + "]", ex);
		}
	}
	
	/**
	 * Adds a {@linkplain KeyFrame} to the {@linkplain Animation} with the specified
	 * {@linkplain Duration} and array of {@linkplain KeyValue}, cancelling the
	 * additional keyframe if there are errors adding the keyframe.
	 * @param time the {@linkplain Duration} time that the KeyFrame is positioned at.
	 * @param values the {@linkplain KeyValue} values that the specific
	 * {@linkplain Animation} holds.
	 * 
	 * @see #addKeyFrame(KeyFrame)
	 */
	public void addKeyFrame(Duration time, KeyValue... values) {
		try {
			timeline.getKeyFrames().add(new KeyFrame(time, values));
		} catch (Exception ex) {
			DebugX.writeErr("Could not add KeyFrame to [Animation" + name + "]", ex);
		}
	}
	
	/**
	 * Gets and returns the name of the {@linkplain Animation}.
	 * @return the name of the {@linkplain Animation}.
	 * @see #setName(String)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets and returns the parent {@linkplain CompositeScreen} of the {@linkplain Animation}.
	 * @return the parent {@linkplain CompositeScreen}.
	 */
	public CompositeScreen getParent() {
		return parent;
	}
	
	/**
	 * Gets and returns the {@linkplain Timeline} that the {@linkplain Animation} is working with.
	 * @return the {@linkplain Timeline} of the {@linkplain Animation}.
	 */
	public Timeline getTimeline() {
		return timeline;
	}
	
	/**
	 * Gets and returns a boolean representing if the instance is working.
	 * @return a boolean representing if the instance is working.
	 */
	public boolean getWorking() {
		return working;
	}
	
	/**
	 * Gets and returns the property that the {@linkplain Animation} is updating throughout
	 * its animation.
	 * @return the property that the {@linkplain Animation} is updating throughout its animation.
	 * @see #setProperty(Observable)
	 */
	public Observable getProperty() {
		return property;
	}
	
	/**
	 * Sets the name of the {@linkplain Animation}.
	 * @param name the new name of the {@linkplain Animation}.
	 * @see #getName()
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Sets the main property of the {@linkplain Animation}.
	 * @param v the property of the {@linkplain Animation}.
	 * @see #getProperty()
	 */
	public void setProperty(Observable v) {
		property = v;
	}
	
	/**
	 * Starts the Animation, setting the value of {@code working} to {@code true},
	 * and starting the timeline from the beginning.
	 * 
	 * @see #stop()
	 */
	public void start() {
		working = true;
		onStart();
		timeline.playFromStart();
	}
	
	/**
	 * Stops the Animation, setting the value of {@code working} to {@code false},
	 * and stopping the timeline completely.
	 * 
	 * @see #start()
	 */
	public void stop() {
		working = false;
		timeline.stop();
	}
	
}
