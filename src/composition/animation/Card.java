package composition.animation;

import composition.annotations.WIP;
import composition.stats.DebugX;

/**
 * This class is used as an optional modifier for Animations, and is not
 * to be used until it is ready.
 * 
 * @author LucioleDeux
 * @since Composite a1.1
 *
 */
@WIP(modified = "2017/07/04", authors = { "LucioleDeux" })
public final class Card {

	// Private variables
	private boolean active = true;
	private int current = 0;
	private int maximum;
	
	// Initialization methods
	/**
	 * Initializes the {@link Card} class and specifies how many cards should
	 * be shown on the screen using the parameter {@code max}.
	 * @param max the {@code integer} representing how many cards should be
	 * shown on the screen.
	 */
	public Card(int max) {
		maximum = max;
	}
	
	
	// Functions for getting properties
	/**
	 * Gets and returns a {@code boolean} that represents if the program is
	 * currently active, and accepting movement to the next card.
	 * @return Returns a {@code boolean} corresponding to whether or not the class
	 * instance is active.
	 */
	public boolean getActive() {
		return active;
	}
	
	/**
	 * Gets and returns an {@code integer} representing which card the screen is
	 * showing.
	 * @return Returns the {@code integer} associated with the current card being
	 * shown to the user.
	 */
	public int getCurrent() {
		return current;
	}
	
	/**
	 * Gets and returns an {@code integer} representing how many cards should be
	 * shown on the screen.
	 * @return Returns an {@code integer} of the maximum amount of cards to be shown.
	 */
	public int getMaximum() {
		return maximum;
	}
	
	// Methods
	public void max() {
		current = maximum;
	}
	
	/**
	 * This method is called to change the screen card into the next card that is
	 * specified, and returns if the operation was successful.
	 * @return Returns {@code true} if the operation was successful.
	 */
	public boolean next() {
		if (!(current == maximum)) {
			current += 1;
		} 

		if (current == maximum) {
			active = false;
			DebugX.writeLine("Card reached maximum.");
			return false;
		}
		
		DebugX.writeLine("Card moved to next card.");
		return true;
	}
	
}
