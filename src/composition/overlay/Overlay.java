package composition.overlay;

import composition.graphics.GraphicsCore;
import composition.screen.CompositeScreen;
import javafx.scene.input.KeyEvent;

public abstract class Overlay {

    private CompositeScreen parent;
    private GraphicsCore core;

    public Overlay(CompositeScreen parent) {
        this.parent = parent;
        this.core = parent.getCore();
    }

    private String name = "New Overlay";

    public Overlay() { }
    public Overlay(String name) { this.name = name; }

    public abstract void onDraw();
    public abstract void onUpdate();
    public abstract void onKeyPressed(KeyEvent e);
    public abstract void onKeyReleased(KeyEvent e);
    public abstract void onKeyTyped(KeyEvent e);

    public GraphicsCore getCore() { return core; }

    public void draw() {
        onDraw();
    }
    public String getName() { return name; }
    public void update() {
        onUpdate();
    }

}
