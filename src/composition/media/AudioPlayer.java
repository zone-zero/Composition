package composition.media;

import composition.stats.DebugX;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

/**
 * This class contains methods, functions, and variables used to play audio in the background
 * of a Composition application.
 * @author LucioleDeux
 *
 */
public class AudioPlayer {

	private boolean repeat;
	private String audio;
	private Media media;
	private MediaPlayer player;
	
	/**
	 * Initializes the {@linkplain AudioPlayer}.
	 * @param file the filename of the file to be played within the {@linkplain AudioPlayer}.
	 */
	public AudioPlayer(String file) {
		try {
			DebugX.writeLine("Setting up [AudioPlayer]...");
			String uri = getClass().getClassLoader().getResource(file).toURI().toString();
			audio = file;
			DebugX.writeLine("Attempting to get [" + uri + "] as Media.");
			media = new Media(uri);
			player = new MediaPlayer(media);
		} catch (Exception e) {
			DebugX.writeErr("Could not play audio file (" + file + "): " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets and returns the name of the file being played within the {@linkplain AudioPlayer}
	 * @return the name of the file being played.
	 */
	public String getFilename() {
		return audio;
	}
	
	/**
	 * Sets the {@linkplain AudioPlayer} to repeat if the parameter is set to {@code true}.
	 * @param x the {@linkplain Boolean} representing if the {@linkplain AudioPlayer} should
	 * repeat.
	 */
	public void setRepeat(boolean x) {
		repeat = x;
		repeatSetter();
	}
	
	private void repeatSetter() {
		player.setOnEndOfMedia(new Runnable() {
			public void run() {
				if (repeat) {
					player.seek(Duration.ZERO);
				} else {
					Thread.currentThread().interrupt();
				}
			}
		});
	}
	
	/**
	 * Starts the {@linkplain AudioPlayer} from its current position.
	 */
	public void play() {
		player.play();
	}
	
	/**
	 * Starts the {@linkplain AudioPlayer} from the beginning.
	 */
	public void playFromStart() {
		player.seek(Duration.ZERO);
		player.play();
	}
	
	/**
	 * Pauses the {@linkplain AudioPlayer} from playing.
	 */
	public void pause() {
		player.pause();
	}
	
	/**
	 * Stops the {@linkplain AudioPlayer} from playing.
	 */
	public void stop() {
		player.stop();
	}
	
	/**
	 * Sets the volume of the {@linkplain AudioPlayer} to the specified volume.
	 * @param v the {@linkplain Double} containing the new volume of the {@linkplain AudioPlayer}.
	 */
	public void setVolume(double v) {
		player.setVolume(v);
	}
	
}
