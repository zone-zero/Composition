package composition.annotations;

/**
 * This interface represents an annotation representing that an object
 * is a <i>work-in-progress</i>.
 * @author LucioleDeux
 *
 */
public @interface WIP {

	/**
	 * The string returned represents the name of the authors who
	 * are maintaining the object.
	 * @return the names of the authors who are maintaining the
	 * object.
	 */
	String[] authors() default "N/A";
	
	/**
	 * The string returned represents when the objected was last
	 * modified.
	 * @return a string form of when the object was last modified.
	 */
	String modified() default "N/A";
	
}
