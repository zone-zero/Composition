package composition.graphics;

import composition.stats.DebugX;
import javafx.geometry.Bounds;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 * This class contains methods and functions that creates functionality with a {@link GraphicsContext} for use on a
 * {@link composition.screen.CompositeScreen CompositeScreen}, enabling an interface to be drawn onto the screen.
 * 
 * @author LucioleDeux
 *
 */
public class GraphicsCore {
	
	private Canvas c;
	private GraphicsContext g;
	private Scene s;
	private Stage st;
	
	/**
	 * Initializes the class with graphics, a scene, and a canvas.
	 * @param gc The object which would be used for drawing.
	 * @param sc The scene which would be used for sizing.
	 * @param ca The container for the object used for drawing.
	 */
	public GraphicsCore(GraphicsContext gc, Scene sc, Canvas ca, Stage sta) {
		c = ca;
		g = gc;
		s = sc;
		st = sta;
	}
	
	/**
	 * Clears the GraphicsContext with the color black.
	 */
	public void clear() {
		try {
			g.setFill(Color.BLACK);
			g.fillRect(0, 0, s.getWidth(), s.getHeight());
		} catch (Exception ex) {
			// DebugX.writeErr("Could not clear the [GraphicsCore]", ex);
			DebugX.writeErr("Could not clear the GraphicsCore", ex);
		}
	}
	
	/**
	 * Clears the GraphicsContext with the {@link Color} specified in {@code c}.
	 * @param c The color that will be used to fill the screen as a clear.
	 */
	public void clear(Color c) {
		try {
			g.setFill(c);
			g.fillRect(0, 0, s.getWidth(), s.getHeight());
		} catch (Exception ex) {
			// DebugX.writeErr("Could not clear the [GraphicsCore]", ex);
			DebugX.writeErr("Could not clear the GraphicsCore", ex);
		}
	}

    /**
     * Clears the GraphicsContext with the {@link Paint} specified in {@code p}.
     * @param p The paint that will be used to fill the screen as a clear.
     */
	public void clear(Paint p) {
		try {
			g.setFill(p);
			g.fillRect(0, 0, s.getWidth(), s.getHeight());
		} catch (Exception ex) {
			//DebugX.writeErr("Could not clear the GraphicsCore", ex);
            DebugX.writeErr("Could not clear the GraphicsCore", ex);
		}
	}
	
	/**
	 * Draws a given {@link javafx.scene.effect.Effect Effect} on the {@link GraphicsCore},
	 * with the Effect specified in the parameter {@code e}. 
	 * @param e the effect that would be drawn on the screen.
	 */
	public void drawEffect(Effect e) {
		try {
			g.setEffect(e);
			g.applyEffect(e);
		} catch (Exception ex) {
			//DebugX.writeErr("Could not draw effect on the GraphicsCore", ex);
            DebugX.writeErr("Could not draw an effect on the GraphicsCore", ex);
		}
	}
	
	/**
	 * Draws an image on the GraphicsContext with the image declared in the
	 * parameter {@code i}. 
	 * @param i the image that would be drawn on the GraphicsContext.
	 * @param x the location of the image on the X-axis.
	 * @param y the location of the image on the Y-axis.
	 */
	public void drawImage(Image i, double x, double y) {
		try {
			g.drawImage(i, x, y);
		} catch (Exception ex) {
			//DebugX.writeErr("Could not draw image on the GraphicsCore", ex);
            DebugX.writeErr("Could not draw image on the GraphicsCore", ex);
		}
	}
	
	/**
	 * Draws text on the GraphicsContext with paint specified.
	 * @param t The text that would be drawn on the GraphicsContext.
	 * @param p The paint used for drawing on the GraphicsContext.
	 */
	public void drawText(String t, Paint p) {
		drawText(t, p, 0, 0);
	}
	
	/**
	 * Draws text on the GraphicsContext with paint and an offset on the X-axis.
	 * @param t The text that would be drawn on the GraphicsContext.
	 * @param p The paint used for drawing on the GraphicsContext.
	 * @param offsetX The offset on the X-axis from the center.
	 */
	public void drawText(String t, Paint p, double offsetX) {
		drawText(t, p, offsetX, 0);
	}
	
	/**
	 * 
	 * Draws text on the GraphicsContext with paint and an offset on the X-axis and Y-axis.
	 * @param t The text that would be drawn on the GraphicsContext.
	 * @param p The paint used for drawing on the GraphicsContext.
	 * @param offsetX The offset on the X-axis from the center.
	 * @param offsetY The offset on the Y-axis from the center.
	 */
	public void drawText(String t, Paint p, double offsetX, double offsetY) {
		try {
			TextAlignment oTA = g.getTextAlign();
			VPos oVP = g.getTextBaseline();
			
			g.setTextAlign(TextAlignment.CENTER);
			g.setTextBaseline(VPos.CENTER);
			drawText(p, t, (Math.round(c.getWidth()) / 2) + offsetX, (Math.round(c.getHeight() - (getTextHeight(t))) / 2) + offsetY);

			g.setTextAlign(oTA);
			g.setTextBaseline(oVP);
		} catch (Exception ex) {
			// DebugX.writeErr("Could not draw text on the GraphicsContext", ex);
            DebugX.writeErr("Could not draw text on the GraphicsCore", ex);
		}
	}
	
	/**
	 * Draws text at a given position and does not function with other functionality.
     * @param p The {@link Paint} that would be used as the fill of the text.
	 * @param t The text that would be drawn onto the screen.
	 * @param x The location on the X-axis where the text would be drawn.
	 * @param y The location on the Y-axis where the text would be drawn.
	 */
	private void drawText(Paint p, String t, double x, double y) {
		Paint oP = g.getFill();
		g.setFill(p);
		g.fillText(t, x, y);
		g.setFill(oP);
	}

    /**
     * Creates a stroked text on a given position and does not function with other functionality.
     * @param p The {@link Paint} that would be used as the stroke paint.
     * @param t The text that would be stroked onto the screen.
     * @param x The location on the X-axis where the text would be drawn.
     * @param y The location on the Y-axis where the text would be drawn.
     */
	private void strokeText(Paint p, String t, double x, double y) {
		Paint oP = g.getStroke();
		g.setStroke(p);
		g.strokeText(t, x, y);
		g.setStroke(oP);
	}
	
	/**
	 * 
	 * Draws text on the GraphicsContext with paint at the designed X-axis and Y-axis.
	 * @param t The text that would be drawn on the GraphicsContext.
	 * @param p The paint used for drawing on the GraphicsContext.
	 * @param x The location on the X-axis where the text would be drawn.
	 * @param y The location on the Y-axis where the text would be drawn.
	 */
	public void drawTextAtPos(String t, Paint p, double x, double y) {
		try {
			TextAlignment oTA = g.getTextAlign();
			VPos oVP = g.getTextBaseline();
			
			g.setTextAlign(TextAlignment.LEFT);
			g.setTextBaseline(VPos.TOP);
			drawText(p, t, x, y);
			
			g.setTextAlign(oTA);
			g.setTextBaseline(oVP);
		} catch (Exception ex) {
			DebugX.writeErr("Could not draw text on the GraphicsContext", ex);
		}
	}
	
	/**
	 * 
	 * Draws text on the GraphicsContext with paint at the designed X-axis and Y-axis.
	 * @param t The text that would be drawn on the GraphicsContext.
	 * @param p The paint used for drawing on the GraphicsContext.
	 * @param x The location on the X-axis where the text would be drawn.
	 * @param y The location on the Y-axis where the text would be drawn.
	 */
	public void strokeTextAtPos(String t, Paint p, double x, double y) {
		try {
			TextAlignment oTA = g.getTextAlign();
			VPos oVP = g.getTextBaseline();
			
			g.setTextAlign(TextAlignment.LEFT);
			g.setTextBaseline(VPos.TOP);
			strokeText(p, t, x, y);
			
			g.setTextAlign(oTA);
			g.setTextBaseline(oVP);
		} catch (Exception ex) {
			DebugX.writeErr("Could not draw text on the GraphicsContext", ex);
		}
	}

	/**
	 * 
	 * Draws text on the GraphicsContext with paint at the designed X-axis and Y-axis.
	 * @param t The text that would be drawn on the GraphicsContext.
	 * @param p The paint used for drawing on the GraphicsContext.
	 * @param x The location on the X-axis where the text would be drawn.
	 * @param y The location on the Y-axis where the text would be drawn.
	 */
	public void drawTextAtPos(String t, Paint p, double x, double y, boolean alignChange) {
		try {
			TextAlignment oTA = g.getTextAlign();
			VPos oVP = g.getTextBaseline();
			
			if (alignChange) {
				g.setTextAlign(TextAlignment.LEFT);
				g.setTextBaseline(VPos.TOP);
			}
			
			drawText(p, t, x, y);
			
			if (alignChange) {
				g.setTextAlign(oTA);
				g.setTextBaseline(oVP);
			}
		} catch (Exception ex) {
			DebugX.writeErr("Could not draw text on the GraphicsContext", ex);
		}
	}
	
	public static Font NEW_FONT_FROM_RESOURCE(String name, double size) {
		try {
			return Font.loadFont(GraphicsCore.class.getResource(name).toExternalForm(), size);
		} catch (Exception ex) {
			DebugX.writeErr("Could not get font from resource", ex);
			return new Font("Arial", size);
		}
	}
	
	public static Font NEW_FONT(String name, double size) {
		return new Font(name, size);
	}
	
	/**
	 * Sets the font that would be used on the {@link javafx.scene.canvas.GraphicsContext GraphicsContext}
	 * to the font defined in the variable {@code f}.
	 * @param f the font that would be set for the {@link javafx.scene.canvas.GraphicsContext GraphicsContext}.
	 */
	public void setFont(Font f) {
		g.setFont(f);
	}

    /**
     * Gets and returns the font that is being used at the time that the method is being called.
     * @return The font that is being used at the time that the method is being called.
     */
	public Font getFont() { return g.getFont(); }
	
	/**
	 * Finds the Canvas which is used for the drawing graphics.
	 * @return the Canvas.
	 */
	public Canvas getCanvas() {
		return c;
	}
	
	/**
	 * 
	 * @return the GraphicsContext used by the program.
	 */
	public GraphicsContext getGraphicsContext() {
		return g;
	}
	
	/**
	 * The height of the GraphicsCore and the Canvas used by the GraphicsCore.
	 * @return the height of the GraphicsCore.
	 */
	public double getHeight() {
		return c.getHeight();
	}
	
	/**
	 * Gets and returns the scene that is holds the GraphicsCore.
	 * @return the scene holding the GraphicsCore.
	 */
	public Scene getScene() {
		return s;
	}
	
	/**
	 * Gets and returns the stage that is being used by the GraphicsCore.
	 * @return the stage being used by the GraphicsCore.
	 */
	public Stage getStage() {
		return st;
	}
	
	public double getTextHeightAverage(String... t) {
		return getTextHeightAverage(getGraphicsContext().getFont(), t);
	}
	
	/**
	 * Gets and returns the average of the text of multiple texts.
	 * @param f The font used to calculate the text height average.
	 * @param t The text used to calculate the text height average.
	 * @return Returns the average height of multiple texts.
	 */
	public static double getTextHeightAverage(Font f, String... t) {
		int numbers = 0;
		double height = 0d;
		for (String x:t) { height += getTextHeight(f, x); numbers += 1;}
		
		return height / numbers;
	}
	
	/**
	 * Gets and returns the width of the text given, using the font currently used by the
	 * GraphicsCore.
	 * @param t the given string that would be measured.
	 * @return a double with the value of the width of the text.
	 */
	public double getTextHeight(String t) {
		return getTextHeight(g.getFont(), t);
	}
	
	/**
	 * Gets and returns the height of text given, assigned with its font.
	 * @param f the Font that would be measured.
	 * @param t the given string that would be measured.
	 * @return a double with the value of the height of the text.
	 */
	public static double getTextHeight(Font f, String t) {
		try {
			Text text = new Text(t);
		    text.setFont(f);
		    Bounds tb = text.getBoundsInLocal();
		    // Rectangle stencil = new Rectangle(tb.getMinX(), tb.getMinY(), tb.getWidth(), tb.getHeight());
		    // Shape intersection = Shape.intersect(text, stencil);
		    // Bounds ib = intersection.getBoundsInLocal();
		    return tb.getHeight();
		} catch (Exception ex) {
			DebugX.writeErr("Could not get the height of the text", ex);
			return 0;
		}
	}

	/**
	 * Gets and returns the average of the text of multiple texts.
	 * @param f The font used to calculate the average width of the text.
	 * @param t The text used to calculate average width.
	 * @return a double with the average of the width.
	 */
	public static double getTextWidthAverage(Font f, String... t) {
		int numbers = 0;
		double width = 0d;
		for (String x:t) { width += getTextWidth(f, x); numbers += 1;}
		
		return width / numbers;
	}
	
	/**
	 * Gets and returns the width of the text given, using the font currently used by the
	 * GraphicsCore.
	 * @param t the given string that would be measured.
	 * @return a double with the value of the width of the text.
	 */
	public double getTextWidth(String t) {
		return getTextWidth(g.getFont(), t);
	}

	/**
	 * Gets and returns the width of the text given, assigned with its font.
	 * @param f the Font that would be measured.
	 * @param t the given string that would be measured.
	 * @return a double with the value of the width of the text.
	 */
	public static double getTextWidth(Font f, String t) {
		try {
			Text text = new Text(t);
		    text.setFont(f);
		    Bounds tb = text.getBoundsInLocal();
		    // Rectangle stencil = new Rectangle(tb.getMinX(), tb.getMinY(), tb.getWidth(), tb.getHeight());
		    // Shape intersection = Shape.intersect(text, stencil);
		    // Bounds ib = intersection.getBoundsInLocal();
		    return tb.getWidth();
		} catch (Exception ex) {
			DebugX.writeErr("Could not get the width of the text", ex);
			return 0;
		}
	}
	
	/**
	 * Gets and returns the width of the GraphicsCore and the Canvas used by the GraphicsCore.
	 * @return the width of the GraphicsCore.
	 */
	public double getWidth() {
		return c.getWidth();
	}
	
}
