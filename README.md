# Composition
**Composition** is a Java resource library using the `composition.*` package prefix. 

## The goal
The goal of Composition is to simplify and create a system of basic classes and interfaces with methods and functions that can simplify using JavaFX and its GraphicsContext component.

## Requirements for Composition
* Eclipse, NetBeans, or similar Java IDEs. *It is important to make sure your IDE supports JavaFX.*
* Java 1.8 with JavaFX.

## Package list
* `composition.animation` - 
* `composition.graphics` - 
* `composition.screen` - 
